# data_rekening = [
#
#     {
#         "id": "1234",
#         "no_rekening": "1234567890",
#         "username": "pery",
#         "pin": "4321",
#         "saldo": 0
#     },
#     {
#         "id": "4321",
#         "no_rekening": "0987654321",
#         "username": "tono",
#         "pin": "1234",
#         "saldo": 25000000
#     }
# ]


# ganti_list = ['mamat', 'ojan']
#
# kosongan = []
#
# for i in data_rekening:
#     for x in ganti_list:
#         kosongan.append(x)
#
# # ini masih list
#
# print(kosongan)
#
# # convert menggunakan tipe data sets
#
# kosongan = set(kosongan)
#
# print(kosongan)
#
# kosongan = list(kosongan)
#
# print(kosongan)


tipe_jeans = [

    {'merek': 'levis',
     'tipe': 'cutbray',
     'warna': 'putih'
     },

    {'merek': 'ACDC',
     'tipe': 'skinny',
     'warna': 'coklat',
     },

    {'merek': 'zara',
     'tipe': 'normal',
     'warna': 'biru'
     },


    {
        'merek': 'billabong',
        'tipe': 'bomber',
        'warna': ['biru', 'coklat', 'hitam']
    }

]

for model in tipe_jeans:
    print("Model:", model['tipe'])
    print(f"warna: {model['warna']}")
    for warna in model['warna']:
        print(warna)
